package au.com.rs.and.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import au.com.rs.and.notification.model.MessageNotification;

public class MainActivity extends ActionBarActivity {

	public static final String TAG_LOG = "Notification";

	private NotificationManager notificationManager;
	private MessageNotification messageNotification;

	public MainActivity() {
		super();
		this.init();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void startNotification(View view) {
		Log.d(TAG_LOG, "Start Notification");

		messageNotification.setTitle("One new message");
		messageNotification.setTicker("Message Alert");
		messageNotification.setContextText("You have received one new message");

		createNotification();
	}

	public void cancelNotification(View view) {
		if (messageNotification != null) {
			notificationManager.cancel(messageNotification.getNotificationId());
		}
	}

	public void updateNotification(View view) {
		messageNotification.setTitle("Updated message");
		messageNotification.setTicker("Updated message ALERT");
		messageNotification
				.setContextText("You have received one new message; updated");

		createNotification();
	}

	public void showAlert(View view) {
		Toast.makeText(this, "Click on screen", Toast.LENGTH_LONG).show();
	}

	private void init() {
		if (messageNotification == null) {
			messageNotification = new MessageNotification();
			messageNotification.setNotificationId(100);
		}
	}

	private void createNotification() {
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
				this);
		notificationBuilder.setContentTitle(messageNotification.getTitle());
		notificationBuilder.setTicker(messageNotification.getTicker());
		notificationBuilder
				.setContentText(messageNotification.getContextText());
		notificationBuilder
				.setNumber(messageNotification.getMessageNumber() + 1);

		notificationBuilder.setSmallIcon(R.drawable.notifications);

		Intent resultIntent = new Intent(this, NotificationView.class);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

		stackBuilder.addParentStack(NotificationView.class);

		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		notificationBuilder.setContentIntent(resultPendingIntent);

		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(messageNotification.getNotificationId(),
				notificationBuilder.build());
		Log.d(TAG_LOG, "Notification has created");
	}

}
