package au.com.rs.and.notification.model;

public class MessageNotification {
	private int notificationId;
	private String title;
	private String contextText;
	private String ticker;
	private int messageNumber;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContextText() {
		return contextText;
	}
	public void setContextText(String contextText) {
		this.contextText = contextText;
	}
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public int getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(int otificationId) {
		this.notificationId = otificationId;
	}
	public int getMessageNumber() {
		return messageNumber;
	}
	public void setMessageNumber(int messageNumber) {
		this.messageNumber = messageNumber;
	}
}
